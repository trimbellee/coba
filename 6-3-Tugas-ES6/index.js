// Soal 1 

// Jawaban Soal 1
const luasPersegiPangjang = (panjang , lebar) => {
    return panjang * lebar
}
const kelilingPersegiPanjang = (panjang ,lebar) =>{
    return 2*panjang + 2*lebar
}
let panjang = 4
let lebar = 5
console.log(`Luas : ${luasPersegiPangjang(panjang, lebar)} Keliling : ${kelilingPersegiPanjang(panjang, lebar)}`)




// Soal 2

// Jawaban Soal 2
const newFunction = (firstName, lastName) =>{
    return {
      firstName,
      lastName,
      fullName: function(){
        console.log(`${firstName} ${lastName}`)
      }
    }
  }
   
  //Driver Code 
  newFunction("William", "Imoh").fullName() 



// Soal 3


// Jawaban Soal 3
const newObject = {
  firstName: "Muhammad",
  lastName: "Iqbal Mubarok",
  address: "Jalan Ranamanyar",
  hobby: "playing football",
}

const {firstName, lastName , address , hobby} = newObject

// Driver code
console.log(firstName, lastName, address, hobby)


// Soal 4

const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
// const combined = west.concat(east)

let combined = [...west,...east] // Jawaban Soal 4
//Driver Code
console.log(combined)


// Soal 5

const planet = "earth" 
const view = "glass" 
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet 

const after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet}` // Jawaban Soal 5

console.log(before)
console.log(after)
